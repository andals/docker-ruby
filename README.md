# docker-ruby

[git](https://gitee.com/andals/docker-ruby)

# Init

```
git clone git@gitee.com:andals/docker-ruby.git ruby
cd ruby
prjHome=`pwd`
rubyVer=2.5.0
```

# Build pkg

```
docker run -i -t -v $prjHome/:/build --name=build-ruby-${rubyVer} andals/centos:7 /bin/bash
/build/script/build_pkg.sh
```

# Build image

```
docker build -t andals/ruby:${rubyVer} ./
```
