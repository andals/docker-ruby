#!/bin/bash

buildRoot=/build
installDstRoot=/usr/local

srcRoot=$buildRoot/src
buildTmpRoot=$buildRoot/tmp
pkgRoot=$buildRoot/pkg
scriptRoot=$buildRoot/script

$scriptRoot/pre_build.sh

rubyVer=2.5.0

cd $installDstRoot
tar zxvf $pkgRoot/ruby-${rubyVer}.tar.gz
ln -s ruby-${rubyVer} ruby

echo -e '\nPATH=$PATH:/usr/local/ruby/bin' >> $HOME/.bashrc
