#!/bin/bash

buildRoot=/build
installDstRoot=/usr/local

srcRoot=$buildRoot/src
buildTmpRoot=$buildRoot/tmp
pkgRoot=$buildRoot/pkg
scriptRoot=$buildRoot/script

$scriptRoot/pre_build.sh

mkdir -p $buildTmpRoot
cd $buildTmpRoot

rubyVer=2.5.0
tar zxvf $srcRoot/ruby-${rubyVer}.tar.gz

cd ruby-${rubyVer}

./configure --prefix=$installDstRoot/ruby-${rubyVer}
make
make install

for extName in `zlib openssl`
do
    cd $srcRoot/ext/$extName
    $installDstRoot/ruby-${rubyVer}/bin/ruby extconf.rb
    sed -i 's/\$(top_srcdir)/..\/../g' Makefile
    make
    make install
done

$installDstRoot/ruby-${rubyVer}/bin/gem update --system
$installDstRoot/ruby-${rubyVer}/bin/gem install rubygems-update
$installDstRoot/ruby-${rubyVer}/bin/update_rubygems

cd $installDstRoot
tar zcvf ruby-${rubyVer}.tar.gz ruby-${rubyVer}
cp ruby-${rubyVer}.tar.gz $pkgRoot
